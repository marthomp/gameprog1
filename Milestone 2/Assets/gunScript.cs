using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gunScript : MonoBehaviour
{
    public GameObject leftBullet;
    public GameObject rightBullet;
    private bool shot;
    private float shotDelay;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
      delayReset();
      GameObject obj = GameObject.FindGameObjectWithTag("GameManager");
      GameManager sm = obj.GetComponent<GameManager>();

      if (Input.GetKey(KeyCode.RightArrow) && !shot) {
        shotDelay = 1f;
        shot = true;
        spawnRightBullet();
      }
      if (Input.GetKey(KeyCode.LeftArrow) && !shot) {
        shotDelay = 1f;
        shot = true;
        spawnLeftBullet();
      }

      if (Input.GetKey(KeyCode.E) && sm.pauseRemote) {
        if(sm.platformsPaused == false) {
          sm.platformsPaused = true;
        } else {
          sm.platformsPaused = false;
        }
      }
    }

    public void spawnLeftBullet() {
      GameObject obj = GameObject.Instantiate<GameObject>(leftBullet);
      SpriteRenderer m_SpriteRenderer = obj.GetComponent<SpriteRenderer>();
      GameObject gm = GameObject.FindGameObjectWithTag("GameManager");
      GameManager sm = gm.GetComponent<GameManager>();
      if(sm.penetrativeProperty) {
        m_SpriteRenderer.color = Color.yellow;
      }
      obj.transform.position = transform.position;
      Destroy(obj, 10);
    }
    public void spawnRightBullet() {
      GameObject obj = GameObject.Instantiate<GameObject>(rightBullet);
      SpriteRenderer m_SpriteRenderer = obj.GetComponent<SpriteRenderer>();
      GameObject gm = GameObject.FindGameObjectWithTag("GameManager");
      GameManager sm = gm.GetComponent<GameManager>();
      if(sm.penetrativeProperty) {
        m_SpriteRenderer.color = Color.yellow;
      }
      obj.transform.position = transform.position;
      Destroy(obj, 10);
    }

    public void delayReset() {
      if(shot && shotDelay > 0) {
        shotDelay -= Time.deltaTime;
      }
      if(shotDelay < 0) {
        shotDelay = 0;
        shot = false;
      }
    }
}
