using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveUp : MonoBehaviour
{
    private GameObject target=null;
    private Vector3 offset;
    public float speed = 5f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        GameObject obj = GameObject.FindGameObjectWithTag("GameManager");
        GameManager sm = obj.GetComponent<GameManager>();
        if(!sm.platformsPaused) {
          transform.Translate(new Vector3(0, speed * Time.deltaTime, 0));
        }
    }

    void OnTriggerStay2D(Collider2D col){

      target = col.gameObject;
      offset = target.transform.position - transform.position;

    }

    void OnTriggerExit2D(Collider2D col){
      target = null;
    }

    void LateUpdate(){
        if (target != null) {
          target.transform.position = transform.position+offset;
        }
    }
}
