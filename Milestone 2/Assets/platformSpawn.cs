using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platformSpawn : MonoBehaviour
{
  public GameObject platform;
    // Start is called before the first frame update

    void spawnPlatform() {
      GameObject obj = GameObject.Instantiate<GameObject>(platform);
      Destroy(obj, 10);
    }

    void Start()
    {
        InvokeRepeating("spawnPlatform", 1.0f, 1.5f);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
