using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject target;
    public float speed;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
      transform.RotateAround(target.transform.position, Vector3.forward, speed * Time.deltaTime);  
    }
}
