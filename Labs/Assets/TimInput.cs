using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimInput : MonoBehaviour
{

  public float maxSpeed;
  public float speed;
  private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        // finds a component of type Animator that's attatched to the gameObject that owns this script
        animator = GetComponent<Animator>();

        if (animator == null) {
          Debug.LogError("No Animator Attatched");
        }

    }

    // Update is called once per frame
    void Update()
    {
        float horizontalAxis = Input.GetAxis("Horizontal");

        //check if the controller is pushed left or right
        if (Mathf.Abs(horizontalAxis) > 0.0f) {

          speed = maxSpeed * horizontalAxis * Time.deltaTime;

          transform.Translate(speed, 0.0f, 0.0f);

          animator.SetBool("IsRunning", true);
        } else { // not pushing controller left or right
          animator.SetBool("IsRunning", false);
        }
    }
}
