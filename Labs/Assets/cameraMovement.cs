using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraMovement : CharacterController2D
{

    public Vector3 velocity = Vector3.zero;
    public float speed = 7f;
    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
      animator = GetComponent<Animator>();

      if (animator == null) {
        Debug.LogError("No Animator Attatched");
      }
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.deltaTime > .1) { return; }


        Vector3 input = Vector3.zero;

        if (Input.GetKey("a")) {  input.x -= 1; }
        if (Input.GetKey("d")) { input.x += 1; }
        if (Input.GetKey("s")) { input.y -= 1; }
        if (Input.GetKey("w")) { input.y += 1; }

        Vector3 movement = Vector3.zero;
        movement += input * speed;
        movement += velocity;

        Move(movement * Time.deltaTime);



        }

    }
