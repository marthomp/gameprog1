using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }
    public float speed = 20f;
    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Translate(speed * Time.deltaTime, 0, 0);
        gameObject.transform.Rotate(0, 0, 50 * Time.deltaTime);
    }
}
