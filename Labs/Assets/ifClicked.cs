using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ifClicked : MonoBehaviour
{
    int down;
    SpriteRenderer m_SpriteRenderer;
    // Start is called before the first frame update
    void Start()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_SpriteRenderer.color = Color.blue;
    }

    void OnMouseDown()
    {
      if (down == 0) {
        m_SpriteRenderer.color = Color.blue;
        down = 1;
      } else {
        m_SpriteRenderer.color = Color.red;
        down = 0;
      }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
