using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValleyScript : MonoBehaviour
{
  public Vector3 pos0;
  public Vector3 pos1;
  public Vector3 scale0 = new Vector3(.3f,.3f,.3f);
  public Vector3 scale1 = new Vector3(2,2,2);

  public Vector3 target;
  public Vector3 targetScale;


    // Start is called before the first frame update
    void Start()
    {
      target = pos1;
      transform.position = pos0;
      targetScale = scale1;
      transform.localScale = scale0;
    }


    // Update is called once per frame
    void Update()
    {
      GameObject obj = GameObject.FindGameObjectWithTag("GameManager");
      GameManager sm = obj.GetComponent<GameManager>();

      transform.position = Vector3.MoveTowards(transform.position, target, sm.movementSpeed * Time.deltaTime);

      if ((transform.position-target).magnitude < .01) {
        if ((target-pos1).magnitude < .01) {
          target = pos0;
        } else {
          target = pos1;
        }
      }

      transform.localScale = Vector3.MoveTowards(transform.localScale, targetScale, sm.scalingSpeed * Time.deltaTime);

      if ((transform.localScale-targetScale).magnitude < .01) { // Can swap out magnitude with sqrmagnitude in case of tons of magnitude calls
        if ((targetScale-scale1).magnitude <.01) {
          targetScale = scale0;
        } else {
          targetScale = scale1;
        }
      }

      transform.Rotate(0,0,sm.rotationSpeed * Time.deltaTime);


    }
}
