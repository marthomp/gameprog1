using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


[System.Serializable] public class SpriteAnim {
  public Sprite[] poses;
  public float animSpeed = 12;
  public Sprite Frame(float time) {
    if (poses == null || poses.Length == 0) { return null; }
    int frame = ((int) time) % poses.Length;
    return poses[frame];
  }
}

public class p1Input : CharacterController2D {


  public float speed = 7f;
  public float jumpPower = 15f;
  public float gravity = 33f;
  public Vector3 velocity = Vector3.zero;
  public float skinWidth = .01f/16f;

  public SpriteAnim idle;
  public SpriteAnim walk;
  public SpriteAnim jump;
  public SpriteAnim fall;



  public bool isGrounded { get { return IsTouching(Vector3.down * skinWidth);}}
  public bool bumpedHead { get { return IsTouching(Vector3.up * skinWidth);}}
  private Animator animator;
  // Start is called before the first frame update
    void Start()
    {
        // finds a component of type Animator that's attatched to the gameObject that owns this script
        animator = GetComponent<Animator>();

        if (animator == null) {
          Debug.LogError("No Animator Attatched");
        }
        DontDestroyOnLoad(this.gameObject);

    }


    // Update is called once per frame
    void Update()
    {
        if (Time.deltaTime > .1) { return; }

        Vector3 input = Vector3.zero;

        if (Input.GetKey("a")) {  input.x -= 1; }
        if (Input.GetKey("d")) { input.x += 1; }


        if (isGrounded) {
          velocity.y = 0;
          if (Input.GetKeyDown("w")) { velocity.y = jumpPower; }
        } else {
          if (velocity.y > 0) {
            if (bumpedHead || Input.GetKeyUp("w")) {
              velocity.y = 0;
            }
          }
          velocity.y -= gravity * Time.deltaTime;
        }

        Vector3 movement = Vector3.zero;
        movement += input * speed;
        movement += velocity;

        Move(movement * Time.deltaTime);
/*          animator.SetBool("IsStanding", true);
          animator.SetBool("IsRunningLeft", false);
          animator.SetBool("IsRunningRight", false); */

          UpdateAnimation(input);



    }

    SpriteRenderer spr;
    SpriteAnim currentAnim;
    float animTime;
    void UpdateAnimation(Vector3 input) {
      if(spr == null) { spr = GetComponent<SpriteRenderer>();}
      if(spr == null) { return; }

      if (input.x > 0 ) { spr.flipX = false; }
      if (input.x < 0 ) { spr.flipX = true; }

      if (isGrounded) {
        if (Mathf.Abs(input.x) > 0) {
          currentAnim = walk;
        }else {
          currentAnim = idle;
        }
      } else {
        currentAnim = jump;
      }

      animTime += currentAnim.animSpeed * Time.deltaTime;

      spr.sprite = currentAnim.Frame(animTime);
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        GameObject obj = GameObject.FindGameObjectWithTag("GameManager");
        GameManager sm = obj.GetComponent<GameManager>();

        if(col.gameObject.tag == "attatchment") {
          sm.penetrativeProperty = true;
          Destroy(col.gameObject);
        }
        if(col.gameObject.tag == "BALL") {
          sm.health--;
          Destroy(col.gameObject);

          if(sm.health == 0) {
            Destroy(gameObject);
          }
        }
        if(col.gameObject.tag == "moveCam" && sm.lvlOneComplete) {
          sm.toLevelTwo = true;
        }

        if(col.gameObject.tag == "pitfall") {
          sm.health = 0;
        }
        if(col.gameObject.tag == "goToBoss") {
          SceneManager.LoadScene("boss");
          transform.position = Vector3.zero;
        }
        if(col.gameObject.tag == "remote") {
          sm.pauseRemote = true;
          Destroy(col.gameObject);
        }
        if(col.gameObject.tag == "rocket") {
          sm.health--;
          Destroy(col.gameObject);
        }

    }

}
