using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballScript : MonoBehaviour
{
    public GameObject ball;
    public int cannonHealth;
    public GameObject smoke1;
    public GameObject smoke2;
    public GameObject splosion;
    public GameObject particle1;
    public GameObject particle2;
    public GameObject particle3;
    // Start is called before the first frame update
    void Start()
    {
      InvokeRepeating("spawnBall", 1.0f, 1.5f);
    }

    // Update is called once per frame
    void Update()
    {


    }

    public void spawnBall() {
      GameObject obj = GameObject.Instantiate<GameObject>(ball);
      Destroy(obj, 10);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
      GameObject obj = GameObject.FindGameObjectWithTag("GameManager");
      GameManager sm = obj.GetComponent<GameManager>();

        if(col.gameObject.tag == "bullet") {
            cannonHealth--;
            if(cannonHealth == 2) {
              GameObject particle1 = Instantiate (smoke1, this.transform.position, Quaternion.identity);
              particle1.GetComponent<ParticleSystem>().Play();
            }
            if(cannonHealth == 1) {
              GameObject particle2 = Instantiate (smoke2, this.transform.position, Quaternion.identity);
              particle2.GetComponent<ParticleSystem>().Play();
            }

            if(cannonHealth == 0) {
              GameObject particle3 = Instantiate (splosion, this.transform.position, Quaternion.identity);
              particle3.GetComponent<ParticleSystem>().Play();

              Destroy(gameObject);
              sm.cannonNum--;
            }
        }


    }


}
