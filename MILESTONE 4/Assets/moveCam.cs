using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveCam : MonoBehaviour
{

    public bool moved = false;
    public bool moved2 = false;
    Vector3 lvlTwo = new Vector3(25,-.3f,-10);
    Vector3 lvlThree = new Vector3(25, -25.3f, -10);
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
      GameObject obj = GameObject.FindGameObjectWithTag("GameManager");
      GameManager sm = obj.GetComponent<GameManager>();
      if(!moved) {
          if(sm.toLevelTwo == true) {
            transform.position = lvlTwo;
            moved = true;
          }
      }
      if(!moved2) {
        if (sm.toLevelThree == true) {
          transform.position = lvlThree;
          moved2 = true;
        }
      }
    }
}
