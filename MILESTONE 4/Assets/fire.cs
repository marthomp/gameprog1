using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fire : MonoBehaviour
{
    public bool top;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(top) {
          transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0));
        } else {
          transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        GameObject obj = GameObject.FindGameObjectWithTag("GameManager");
        GameManager sm = obj.GetComponent<GameManager>();

        if(col.gameObject.tag == "BALL") {
          if(!sm.penetrativeProperty) {
            Destroy(gameObject);
          }
        }

    }

}
