using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class heatSeeking : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject target;
    public float speed;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);

    }

    void OnTriggerEnter2D(Collider2D col)
    {

        if(col.gameObject.tag == "bullet") {
            Destroy(this.gameObject);
          
        }

    }
}
