using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballScript : MonoBehaviour
{
    public GameObject ball;
    public int cannonHealth;
    // Start is called before the first frame update
    void Start()
    {
      InvokeRepeating("spawnBall", 1.0f, 1.5f);
    }

    // Update is called once per frame
    void Update()
    {


    }

    public void spawnBall() {
      GameObject obj = GameObject.Instantiate<GameObject>(ball);
      Destroy(obj, 10);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
      GameObject obj = GameObject.FindGameObjectWithTag("GameManager");
      GameManager sm = obj.GetComponent<GameManager>();

        if(col.gameObject.tag == "bullet") {
            cannonHealth--;

            if(cannonHealth == 0) {
              Destroy(gameObject);
              sm.cannonNum--;
            }
        }


    }


}
