using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public int health;
    public bool penetrativeProperty;
    public int cannonNum;
    public bool lvlOneComplete = false;
    public bool arrowSpawned = false;
    public GameObject directionArrow;
    public bool toLevelTwo = false;
    public bool toLevelThree = false;
    public bool pauseRemote;
    public bool platformsPaused = false;
    // Start is called before the first frame update
    void spawnArrow() {
      GameObject obj = GameObject.Instantiate<GameObject>(directionArrow);
      Destroy(obj, 15f);
    }

    void Start()
    {
      DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
      if (cannonNum == 0) {
        lvlOneComplete = true;
        if(!arrowSpawned) {
          spawnArrow();
          arrowSpawned = true;
        }

      }
      if(health == 0) {
        Application.LoadLevel(0);
      }
    }
}
